# PHP Coding Standard
Custom coding standard for [PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)

## Table of contents
1. [Sniffs included in this standard](#sniffs-included-in-this-standard)
    - [Custom Sniffs](#custom-sniffs)
    - [CodeSniffer Standard Sniffs](#codesniffer-standard-sniffs) 
2. [Installation](#installation)
3. [How to run the sniffs](#how-to-run-the-sniffs)
4. [Fixing errors automatically](#fixing-errors-automatically)

## Sniffs included in this standard

### PHP PSR
- **[PSR1](https://www.php-fig.org/psr/psr-1/)**
- **[PSR2](https://www.php-fig.org/psr/psr-2/)**

#### Custom Sniffs
<details>
<summary>Show all sniffs</summary>
<p>

**No sniffs yet**

</p>
</details>

#### CodeSniffer Standard Sniffs
<details>
<summary>Show all sniffs</summary>
<p>

**Generic.Arrays.DisallowLongArraySyntaxSniff**
- Bans the use of the PHP long array syntax.

**Generic.Arrays.ArrayIndent**
- Ensures that array are indented one tab stop.

**Generic.Classes.DuplicateClassNameSniff**
- Reports errors if the same class or interface name is used in multiple files.
 
**Generic.CodeAnalysis.AssignmentInCondition**
- Detects variable assignments being made within conditions.

**Generic.CodeAnalysis.EmptyStatement**
- This sniff class detected empty statement.
- This sniff implements the common algorithm for empty statement body detection. A body is considered as empty if it is completely empty or it only contains whitespace characters and/or comments.

**Generic.CodeAnalysis.ForLoopWithTestFunctionCall**
- Detects for-loops that use a function call in the test expression.
- This rule is based on the PMD rule catalog. Detects for-loops that use a function call in the test expression.

**Generic.CodeAnalysis.JumbledIncrementer**
- Detects incrementer jumbling in for loops.
- This rule is based on the PMD rule catalog. The jumbling incrementer sniff detects the usage of one and the same incrementer into an outer and an inner loop. Even it is intended this is confusing code.

**Generic.CodeAnalysis.UnconditionalIfStatement**
- Detects unconditional if- and elseif-statements.

**Generic.Commenting.DocComment**
- Ensures doc blocks follow basic formatting.

**Generic.Commenting.Todo**
- Warns about TODO comments.

**Generic.Files.ByteOrderMark**
- A simple sniff for detecting BOMs that may corrupt application work.

**Generic.Files.EndFileNewline**
- Ensures the file ends with a newline character.

**Generic.Files.LineLength**
- Checks the length of all lines in a file.
- properties:
    - `lineLimit = 100`
    - `absoluteLineLimit = 120`
    - `ignoreComments = false`

**Generic.Files.OneObjectStructurePerFile**
- Checks that only one object structure is declared per file.

**Generic.Formatting.DisallowMultipleStatements**
- Ensures each statement is on a line by itself.

**Generic.Functions.FunctionCallArgumentSpacing**
- Checks that calls to methods and functions are spaced correctly.

**Generic.NamingConventions.ConstructorName**
- Bans PHP 4 style constructors.
- Favor PHP 5 constructor syntax, which uses "function __construct()".
- Avoid PHP 4 constructor syntax, which uses "function ClassName()".

**Generic.NamingConventions.UpperCaseConstantName**
- Ensures that constant names are all uppercase.

**Generic.PHP.BacktickOperator**
- Bans the use of the backtick execution operator.

**Generic.PHP.CharacterBeforePHPOpeningTag**
- Checks that the opening PHP tag is the first content in a file.

**Generic.PHP.DeprecatedFunctions**
- Discourages the use of deprecated PHP functions.

**Generic.PHP.DiscourageGoto**
- Discourage the use of the PHP `goto` language construct.

**Generic.PHP.ForbiddenFunctions**
- Discourages the use of alias functions.
- Alias functions are kept in PHP for compatibility with older versions. Can be used to forbid the use of any function.

**Generic.PHP.LowerCaseKeyword**
- Checks that all PHP keywords are lowercase.

**Generic.PHP.NoSilencedErrors**
- Throws an error or warning when any code prefixed with an asperand is encountered.

**Generic.Strings.UnnecessaryStringConcat**
- Checks that two strings are not concatenated together; suggests using one string instead.

**Generic.WhiteSpace.ArbitraryParenthesesSpacing**
- Check & fix whitespace on the inside of arbitrary parentheses.
- Arbitrary parentheses are those which are not owned by a function (call), array or control structure.
- Spacing on the outside is not checked on purpose as this would too easily conflict with other spacing rules.

**Generic.WhiteSpace.DisallowTabIndent**
- Throws errors if tabs are used for indentation.

**Generic.WhiteSpace.LanguageConstructSpacing**
- Ensures all language constructs contain a single space between themselves and their content.

**Squiz.Arrays.ArrayBracketSpacing**
- Ensure that there are no spaces around square brackets.

**Squiz.Classes.LowercaseClassKeywords**
- Ensures all class keywords are lowercase.

**Squiz.Classes.SelfMemberReference**
- Tests self member references.
- Verifies that:
    - `self::` is used instead of `Self::`
    - `self::` is used for local static member reference
    - `self::` is used instead of `self ::`

**Squiz.Classes.ValidClassName**
- Ensures classes are in camel caps, and the first letter is capitalised.

**Squiz.ControlStructures.ControlSignature**
- Verifies that control statements conform to their coding standards.

**Squiz.ControlStructures.ElseIfDeclaration**
- Ensures the use of else if over elseif.

**Squiz.ControlStructures.ForEachLoopDeclaration**
- Verifies that there is a space between each condition of foreach loops.

**Squiz.ControlStructures.ForLoopDeclaration**
- Verifies that there is a space between each condition of for loops.

**Squiz.ControlStructures.InlineIfDeclaration**
- Tests the spacing of shorthand IF statements.

**Squiz.ControlStructures.LowercaseDeclaration**
- Ensures all control structure keywords are lowercase.

**Squiz.ControlStructures.SwitchDeclaration**
- Enforces switch statement formatting.

**Squiz.NamingConventions.ValidVariableName**
- Checks the naming of variables and member variables. (camelCase)

**Squiz.Operators.ComparisonOperatorUsage**
- A Sniff to enforce the use of IDENTICAL type operators rather than EQUAL operators.

**Squiz.Operators.ValidLogicalOperators**
- Ensures logical operators 'and' and 'or' are not used.

**Squiz.PHP.Eval**
- The use of eval() is discouraged.

**Squiz.PHP.GlobalKeyword**
- Stops the usage of the "global" keyword.

**Squiz.PHP.NonExecutableCode**
- Warns about code that can never been executed.

**Squiz.Scope.MemberVarScope**
- Verifies that class members have scope modifiers.

**Squiz.Scope.MethodScope**
- Verifies that class methods have scope modifiers.

**Squiz.Scope.StaticThisUsageSniff**
- Checks for usage of $this in static methods, which will cause runtime errors.

**Squiz.Strings.ConcatenationSpacing**
- Makes sure there are no spaces around the concatenation operator.

**Squiz.WhiteSpace.MemberVarSpacing**
- Verifies that class members are spaced correctly.

**Squiz.WhiteSpace.ObjectOperatorSpacing**
- Ensure there is no whitespace before/after an object operator.

**Squiz.WhiteSpace.OperatorSpacing**
- Verifies that operators have valid spacing surrounding them.

**Squiz.WhiteSpace.ScopeKeywordSpacing**
- Ensure there is a single space after scope keywords.

**Squiz.WhiteSpace.SemicolonSpacing**
- Ensure there is no whitespace before a semicolon.

**Squiz.WhiteSpace.SuperfluousWhitespace****
- Checks for unneeded whitespace.
- Checks that no whitespace precedes the first content of the file, exists after the last content of the file, resides after content on any line, or are two empty lines in functions.

</p>
</details>

## Installation
1. Import this repository to project via composer repository
    ```
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.com/Pernick/php-coding-standard"
        }
    ]
    ```
2. Require imported repository
    ```
    "require-dev": {
        "pernick/php-coding-standard": "dev-master"
    }
    ```
3. Run `composer update` to install dependency

## How to run the sniffs
### PHPStorm
1. Link `phpcs` executable to PHPStorm
![PHPStorm PHPCS executable](resources/phpcs-executable.png)
![PHPStorm PHPCS executable path](resources/phpcs-executable-path.png)
2. Enable PHPStorm inspections with this coding standard
![PHPStorm PHPCS inspections](resources/phpcs-inspections.png)


## Fixing errors automatically
- It is possible to automatically correct certain types of errors/warnings via `phpcbf` executable
- Automatically fixing errors is generally discouraged and **should always be manually reviewed**
